const BASE_URL = "https://62de275579b9f8c30ab3f077.mockapi.io";
let DSSV = [];
function getDSSV() {
  batLoading();
  axios({
    url: `${BASE_URL}/sv`,
    method: "GET",
  })
    .then(function (res) {
      tatLoading();
      console.log(res);
      renderDSSV(res.data);
      DSSV = res.data;
    })
    .catch(function (err) {
      tatLoading();
      console.log(err);
    });
}

getDSSV();

function xoaSinhVien(id) {
  axios({
    url: `${BASE_URL}/sv/${id}`,
    method: "DELETE",
  })
    .then(function (res) {
      console.log(res);
      getDSSV();
    })
    .catch(function (err) {
      console.log(err);
    });
}

function themSV() {
  // new SV object lấy từ form
  let newSV = {
    name: "Bánh mì",
    email: "Ceasar3@hotmal.com",
    password: "Cdsv3e5dvgd",
    math: 4,
    physics: 8,
    chemistry: 9,
  };

  batLoading();
  axios({
    url: `${BASE_URL}/sv`,
    method: "POST",
    data: newSV,
  })
    .then(function (res) {
      tatLoading();
      console.log(res);
    })
    .catch(function (err) {
      tatLoading();
      console.log(err);
    });
}

function suaSV() {
  // lay thong tin tu form
  const id = document.getElementById("txtMaSV").value;
  const name = document.getElementById("txtTenSV").value;
  const email = document.getElementById("txtEmail").value;
  const matkhau = document.getElementById("txtPass").value;
  const math = document.getElementById("txtDiemToan").value;
  const physic = document.getElementById("txtDiemLy").value;
  const chemistry = document.getElementById("txtDiemHoa").value;
  batLoading();

  axios({
    url: `${BASE_URL}/sv/${id}`,
    method: "PUT",
    data: {
      name: name,
      email: email,
      password: matkhau,
      math: math,
      physic: physic,
      chemistry: chemistry,
    },
  })
    .then(function (res) {
      // Tai lai danh sach sinh vien de cap nhat thong tin
      getDSSV();
      console.log(res);
    })
    .catch(function (err) {
      tatLoading();
      console.log(err);
    });
}

function taiThongTinLenForm(idSV = "") {
  // tim thong tin sv dua tren id truyen vao
  const sv = DSSV.find((sv) => sv.id === idSV);

  // Tai thong tin len form
  document.getElementById("txtMaSV").value = sv.id;
  document.getElementById("txtMaSV").setAttribute("disabled", true);
  document.getElementById("txtTenSV").value = sv.name;
  document.getElementById("txtEmail").value = sv.email;
  document.getElementById("txtPass").value = sv.password;
  document.getElementById("txtDiemToan").value = sv.math;
  document.getElementById("txtDiemLy").value = sv.physic;
  document.getElementById("txtDiemHoa").value = sv.chemistry;
}

function resetSinhVien() {
  document.getElementById("txtMaSV").value = "";
  document.getElementById("txtTenSV").value = "";
  document.getElementById("txtEmail").value = "";
  document.getElementById("txtPass").value = "";
  document.getElementById("txtDiemToan").value = "";
  document.getElementById("txtDiemLy").value = "";
  document.getElementById("txtDiemHoa").value = "";
}
